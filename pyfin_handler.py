#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

Created on Wed Aug 22 12:57:23 2018

@author: herman
"""

from pyfin import Portfolio, Person, TFSA, DI, RA
import pandas as pd
import numpy as np
import simplejson as json


import pandas as pd
import simplejson as json

def generateInput():
    df = pd.read_excel('input.xlsx',
                           index_col=0)
    df['Financial Year'] = df.index
    df = df[df.columns].astype(str)
    payload = {}

    payload['input_data'] = df.to_dict('records')

    payload['person'] = {'dob': '1987-02-05',
              'ibt': '45000', #Income before tax
              'expenses': '19000', #Expenses
              'monthly_med_aid_contr' : '2583.18', #Monthly medical aid
              'ma_dependants': '2', #Medical aid dependants
              'medical_expenses': '9000', #Ander medical expenses (Jaarliks)
              'era': '65', #  Expected retirement age
              'le': '95', #  Life expectancy
              'strategy': 'optimal'}
    payload['investments'] = {}
    payload['investments']['tfsa'] = {'type': 'tfsa',
                'initial':'33000',
                'growth':'12',
                'ctd':'0', # Contributions to date.
                'ytd':'0'} # Hierdie finansieele jaar

    payload['investments']['ra'] = {'type': 'ra',
            'initial':'10000',# Beginwaarde
            'ra_growth':'12', # retirement annuity growth
            'la_growth':'12', # living annuity growth
            'ytd':'0',     # Year to date
            'payout_fraction':'0.3'} # Payout fraction. How much to take from this fund as a lump sum.

    payload['investments']['di'] = {'type': 'di',
            'initial':'10000',
            'growth':'12',
            'cg_to_date':'0'} # Capital gain to date

    payload['email'] = 'hermancarstens@gmail.com'
    payload['description'] = 'My test plan'
    payload['session_id'] = '123456'
    return payload





# Portfolio
payload = generateInput()
herman = Person(dob=payload['person']['dob'],
              ibt=payload['person']['ibt'], #Income before tax
              expenses=payload['person']['expenses'], #Expenses
              monthly_med_aid_contr=payload['person']['monthly_med_aid_contr'], #Monthly medical aid
              ma_dependants=payload['person']['ma_dependants'], #Medical aid dependants
              medical_expenses=payload['person']['medical_expenses'], #Ander medical expenses (Jaarliks)
              era=payload['person']['era'], #Expected retirement age
              le=payload['person']['le'], #Life expectancy
              strategy=payload['person']['strategy']) #Optimal of safe.


#df_input = pd.read_excel('input.xlsx',
#                           index_col=0)
#df_input.info()

p = Portfolio(herman, pd.DataFrame(payload['input_data']))

tfsa = TFSA(herman,
            initial=payload['investments']['tfsa']['initial'],
            growth=payload['investments']['tfsa']['growth'],
            ctd=payload['investments']['tfsa']['ctd'], # Contributions to date.
            ytd=payload['investments']['tfsa']['ytd']) # Hierdie finansieele jaar

ra = RA(herman,
        initial=payload['investments']['ra']['initial'],# Beginwaarde
        ra_growth=payload['investments']['ra']['ra_growth'], # retirement annuity growth
        la_growth=payload['investments']['ra']['la_growth'], # living annuity growth
        ytd=payload['investments']['ra']['ytd'],     # Year to date
        payout_fraction=payload['investments']['ra']['payout_fraction']) # Payout fraction. How much to take from this fund as a lump sum.

di = DI(herman,
        initial=payload['investments']['di']['initial'],
        growth=payload['investments']['di']['growth'],
        cg_to_date=payload['investments']['di']['cg_to_date']) # Capital gain to date

contr_TFSA = pd.Series(index=tfsa.df.index, name='contr',
                       data=33000*np.ones(tfsa.df.shape[0]))
contr_DI = pd.Series(index=tfsa.df.index, name='contr',
                     data=15000*np.ones(di.df.shape[0]))
contr_RA = pd.Series(index=tfsa.df.index, name='contr',
                     data=0*np.ones(ra.df.shape[0]))


contr_TFSA = pd.Series(index=tfsa.df.index, name='contr',
                       data=contr_TFSA)
contr_DI = pd.Series(index=tfsa.df.index, name='contr',
                     data=contr_DI)
contr_RA = pd.Series(index=tfsa.df.index, name='contr',
                     data=contr_RA)


withdrawals_TFSA = pd.Series(index=tfsa.df.index,
                        name='withdrawals',
                        data=33000*np.ones(tfsa.df.shape[0]))
withdrawals_DI = pd.Series(index=tfsa.df.index,
                        name='withdrawals',
                        data=50000*np.ones(tfsa.df.shape[0]))
withdrawals_RA = pd.Series(index=tfsa.df.index,
                        name='withdrawals',
                        data=0*np.ones(tfsa.df.shape[0]))

contr_TFSA.iloc[15:] = 0
contr_DI.loc[15:] = 0
contr_RA.loc[15:] = 0

withdrawals_DI.loc[p.df.index[0]:p.retirement_fy_end] = 0
withdrawals_RA.loc[p.df.index[0]:p.last_working_year] = 0
withdrawals_TFSA.loc[p.df.index[0]:] = 0

ra.calculateOptimalWithdrawal(contr_RA)
tfsa.calculateOptimalWithdrawal(contr_TFSA)
di.calculateOptimalWithdrawal(contr_TFSA)

p.addInvestment('RA', ra)
p.addInvestment('DI', di)
p.addInvestment('TFSA', tfsa)

p.contr = pd.DataFrame(index=p.df.index,
                          columns=p.investment_names)

for i in p.investment_names:
    p.contr[i] = p.investments[i].df.contr


p.calculate()
p.calculate(p.taxEfficientPosTFSADIFirst())
df_di = di.df
df_ra = ra.df
df_tfsa = tfsa.df
df_p = p.df

print('Mean IAT, current contributions: R', round(df_p.loc[p.retirement_fy_end:, 'iat'].mean(), 2))
#a =  p.plot()



#%%
p.optimize(reduced_expenses=True)
df_p = p.df
a = p.plot()
