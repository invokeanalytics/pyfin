#!flask/bin/python
from flask import Flask, request, url_for, jsonify, abort, make_response
from flask_cors import CORS
app = Flask(__name__)
CORS(app)

import simplejson as json
import pyfin
import os

@app.route('/calculate', methods=['GET'])
def return_calculate():
    valid, payload = pyfin.generateReport(request.json)
    if valid:
        return make_response(json.dumps(payload), 200)

    else:    
        return make_response(json.dumps(payload), 406) 
    
    
@app.route('/validate', methods=['GET'])
def return_validate():
    valid = pyfin.validate(request.json)
    if valid:
        return make_response('200')

    else:    
        return make_response('406') 
    
@app.route('/api/validate', methods=['POST'])
def return_calculate_dawie():
    return make_response(json.dumps({'message':"I'm getting something back", 'status':200}), 200)


if __name__ == '__main__':
    port = int(os.environ.get("PORT", 33507))
    app.run(host='0.0.0.0', port=port, debug=True)
