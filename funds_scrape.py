#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 19:57:17 2019

@author: herman
"""

from bs4 import BeautifulSoup
import pandas as pd
import requests
import time
from tqdm import tqdm
import numpy as np
import PyPDF2
import io

#  Scraping:
r = requests.get('http://www.fundsdata.co.za/navs/')
soup = BeautifulSoup(r.text, 'lxml')
url_list = []
for i in soup.findAll('a')[2:]:
    url_list.append(i.get('href'))
    
df = pd.read_html(f'http://www.fundsdata.co.za/navs/{url_list[0]}', header=1)[0].iloc[0:-1]

for i in tqdm(url_list[1:]):
    try:
        df_page = pd.read_html(f'http://www.fundsdata.co.za/navs/{i}', header=1)[0].iloc[0:-1]
        df = df.append(df_page)
    except:  #  some links are broken.
        pass
    time.sleep(2)
   
#  Cleaning:
df = df.drop(df.loc[df['Fund Name'] == 'Profile Data Average'].index)
df = df.iloc[0:-4]
df = df.drop_duplicates(subset='Fund Name')

df['index'] = range(df.shape[0])
df = df.set_index('index')
for i in df.index:
    try:
        float(df.loc[i, 'InitialFee(%)'])
    except ValueError:
        df = df.drop(i)
        
    if i in df.index and np.isnan(float(df.loc[i, 'InitialFee(%)'])):
        df = df.drop(i)
        
df['index'] = range(df.shape[0])
df = df.set_index('index')
lst_cash_values = ['5 yearsCash Value', 
    '3 YearsCash Value',
    '1 YearCash Value',
    '6 MonthsCash Value']
df[lst_cash_values] = df[lst_cash_values].astype(float)

def averageGrowth(s):
    if not np.isnan(s.loc['5 yearsCash Value']):
        five_year_growth = (s.loc['5 yearsCash Value'])/100
        three_year_growth = (s.loc['3 YearsCash Value'])/100
        six_month_growth = (s.loc['6 MonthsCash Value'])/100
        ann_oyg = s.loc['1 YearCash Value']/100 - 1
        ann_fyg = five_year_growth**(1/5) - 1
        ann_tyg = three_year_growth**(1/3) - 1
        ann_smg = six_month_growth**(1/0.5) - 1
        
        return (ann_fyg*5 + ann_tyg*3 + ann_oyg + 0.5*ann_smg)/9.5
        
                      
    elif not np.isnan(s.loc['3 YearsCash Value']):
        three_year_growth = (s.loc['3 YearsCash Value'])/100
        six_month_growth = (s.loc['6 MonthsCash Value'])/100
        ann_oyg = s.loc['1 YearCash Value']/100 - 1
        ann_tyg = three_year_growth**(1/3) - 1
        ann_smg = six_month_growth**(1/0.5) - 1
        
        return (ann_tyg*3 + ann_oyg + 0.5*ann_smg)/4.5

    elif not np.isnan(s.loc['1 YearCash Value']):
        ann_oyg = s.loc['1 YearCash Value']/100 - 1
        six_month_growth = (s.loc['6 MonthsCash Value'])/100
        ann_smg = six_month_growth**(1/0.5) - 1
        
        return (ann_oyg + 0.5*ann_smg)/1.5

    elif not np.isnan(s.loc['6 MonthsCash Value']):
        six_month_growth = (s.loc['6 MonthsCash Value'])/100
        return six_month_growth**2

df['ave_growth'] = df.apply(averageGrowth, axis=1)

df = df.loc[df['ave_growth'] > -0.2]

df = df.dropna(subset=['ave_growth'])
df_ra = df.loc[df['Fund Name'].str.startswith('*')]
df_ra.loc[:, 'index'] = range(df_ra.shape[0])
df_ra = df_ra.set_index('index')

#  10X
url = 'https://www.10x.co.za/assets/downloads/Retirement%20Fund%20Investment%20Report%20-%20March%202019%20Final.pdf'
last_pdf = requests.get(f'https://www.10x.co.za/assets/downloads/Retirement%20Fund%20Investment%20Report%20-%20March%202019%20Final.pdf').content
      
#  Use latest pdf to mine info
read_pdf = PyPDF2.PdfFileReader(io.BytesIO(last_pdf))
page1 = read_pdf.getPage(0).extractText()
   #  Extract 10 year growth from pdf text:
for i in ['High Equity',
          'Medium Equity',
          'Low Equity',
          'Defensive']:
    var = float(page1[page1.index(i):page1.index(i)+60].split('\n')[7].split('%')[0])
    #  Append to existing dataframes
    df = df.append({'Fund Name': '10X ' + i,
               'ave_growth': var/100}, ignore_index=True)
    df_ra = df_ra.append({'Fund Name': '10X ' + i,
               'ave_growth': var/100}, ignore_index=True)

def removeAsterisk(s):
    if s.startswith('*'):
        s = s[1:]
    return s

df_ra['Fund Name'] = df_ra['Fund Name'].map(removeAsterisk)
df['Fund Name'] = df['Fund Name'].map(removeAsterisk)
df.to_csv('Fund_data_all.csv')
df_ra.to_csv('Fund_data_RA.csv')
